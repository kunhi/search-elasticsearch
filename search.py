
#for environ variable
import os 

# youtube api python libraris
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError

# constant variables
DEVELOPER_KEY = os.environ.get("YOUTUBE_API_KEY")
YOUTUBE_API_SERVICE_NAME = 'youtube'
YOUTUBE_API_VERSION = 'v3'

# set call APIs here
youtube = build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION,
    developerKey=DEVELOPER_KEY)

# waiting to pick keyword
search_keyword = input("you can enter the search keyword: ")


# Call the search.list method to retrieve results matching the specified
# query term.
search_response = youtube.search().list(
q=search_keyword,
part='id,snippet',
maxResults=10
).execute()

# check result none or not
if(search_response.get('pageInfo').get('totalResults')):
	# process and customise the results
	for each_dict in search_response.get('items',[]):
		print({
			"videoId":each_dict.get('id').get('videoId'), 
			"title":each_dict.get('snippet').get('title'),
			"url":"https://www.youtube.com/watch?v="+each_dict.get('id').get('videoId')
			})
else:
	print("No result.")		