
import json 
from elasticsearch import Elasticsearch

es=Elasticsearch([{'host':'localhost','port':9200}])


#...........Get total count...............
res = es.search(
		index="amazon_review",
		size=0,
		body={
			"aggs": {
		    "total_record": {
		      "sum": {
		        "field": "count.customer_review_count", 
		      }
		    }
		  }
		})

total_count = res.get("aggregations").get("total_record").get("value")

print("\nTotal number of records {count}\n\n".format(count=int(total_count)))
#...........Get total count end...............

#...........Get top ten reviewers..............
res = es.search(
       index="amazon_review", 
       body={
        "size" : 10,
        "sort" : [
	       {
	          "count.customer_review_count" : {
	             "order" : "desc",
	          }
	       }
	    ]
    }
)

print("____Top ten reviewers________\n")

for each_dict in res.get("hits").get("hits"):
	print("{customer_name} {review_count}".format(customer_name=each_dict.get("_source").get('customer_name'),
		review_count=each_dict.get("_source").get("count").get("customer_review_count")))

print("\n____Top ten reviewers end________\n\n")
#...........Get top ten reviewers end..............

#...........Get top ten products which had reviews........
res = es.search(
       index="product_details", 
       body={
        "size" : 10,
        "sort" : [
	       {
	          "count.product_review_count" : {
	             "order" : "desc",
	          }
	       }
	    ]
    }
)

print("____Top ten product which had reviews________\n")

for each_dict in res.get("hits").get("hits"):
	print("Product Id {product_id} Product Review Count {product_review_count} Average Rating {average_rating}".format(product_id=each_dict.get("_source").get("product_id"),
		product_review_count=each_dict.get("_source").get("count").get("product_review_count"), average_rating=sum(each_dict.get("_source").get("rating_list"))/len(each_dict.get("_source").get("rating_list"))))

print("\n____Top ten product which had reviews end________\n\n")
#...........Get top ten products which had review end...........






