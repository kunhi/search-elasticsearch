

import json 

from elasticsearch import Elasticsearch


with open('Beauty_5.json',) as f:
	content = f.readlines()

es=Elasticsearch([{'host':'localhost','port':9200}])


#..........Add data to DB..............

customer_reviews_count = {}

product_reviews_count = {}

for index, each in enumerate(content):

	if(json.loads(each).get('reviewerID') not in customer_reviews_count.keys()):
		customer_reviews_count[json.loads(each).get('reviewerID')] = 1
	else:
		customer_reviews_count[json.loads(each).get('reviewerID')] = customer_reviews_count.get(json.loads(each).get('reviewerID'))+1 	

	if(json.loads(each).get('asin') not in product_reviews_count.keys()):
		product_reviews_count[json.loads(each).get('asin')] = 1
	else:
		product_reviews_count[json.loads(each).get('asin')] = product_reviews_count.get(json.loads(each).get('asin'))+1		
	
	action_list = []

	res = es.index(index='amazon_review',doc_type='customer_review',id=json.loads(each).get('reviewerID'),body={
		"customer_id":json.loads(each).get('reviewerID'),
		"customer_name":json.loads(each).get('reviewerName'),
		"count":{"customer_review_count":customer_reviews_count.get(json.loads(each).get('reviewerID'))}
		})
	try:
		res_2 = es.get(index="product_details", id=json.loads(each).get("asin"))
	except:
		res_2 = None
		
	if res_2:
		
		res_2.get('_source').get('rating_list').append(json.loads(each).get('overall'))

		response = es.update(
			index="product_details",
			doc_type="product_review",
			id=json.loads(each).get("asin"),
			body={
			"doc":
				{
					"rating_list":res_2.get('_source').get('rating_list'),
					"count":{"product_review_count":product_reviews_count.get(json.loads(each).get('asin'))}					
				}
			}	
		)
	else:		
		product_add = es.index(index='product_details',doc_type='product_review',id=json.loads(each).get("asin"),body={
			"product_id":json.loads(each).get("asin"),
			"rating_list":[json.loads(each).get("overall")],
			"count":{"product_review_count":product_reviews_count.get(json.loads(each).get('asin'))}
			})		

#...........Add data to DB end here..........